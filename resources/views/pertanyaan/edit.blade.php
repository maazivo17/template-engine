@extends('adminlte.master')

@section('title')
<h1>EDIT PERTANYAAN</h1>
@endsection

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit judul dan pertanyaan nomor {{$question->id}}</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan/{{$question->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label>Judul Pertanyaan</label>
                <input type="text" id="title" name="title" class="form-control" placeholder="Judul Anda" value="{{$question->judul}}">
            </div>
            <div class="form-group">
                <label>Isi Pertanyaan</label>
                <textarea class="form-control" id="body" name="body" placeholder="Apa yang anda tanyakan!">{{$question->isi}}</textarea>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-warning">Update</button>
        </div>
    </form>
</div>
@endsection