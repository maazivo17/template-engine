@extends('adminlte.master')

@section('title')
<h1>BUAT PERTANYAAN</h1>
@endsection

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Isikan judul dan pertanyaan yang sesuai anda inginkan!</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/pertanyaan" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label>Judul Pertanyaan</label>
                <input type="text" id="title" name="title" class="form-control" placeholder="Judul Anda">
            </div>
            <div class="form-group">
                <label>Isi Pertanyaan</label>
                <textarea class="form-control" id="body" name="body" placeholder="Apa yang anda tanyakan!"></textarea>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Buat</button>
        </div>
    </form>
</div>
@endsection