@extends('adminlte.master')

@section('title')
<h1>DETAIL DATA</h1>
@endsection

@section('content')
<h4>{{$question->judul}}</h4>
<p>{{$question->isi}}</p>
<a href="/pertanyaan" class="btn btn-secondary">Kembali</a>
@endsection