@extends('adminlte.master')

@section('title')
<h1>DAFTAR PERTANYAAN</h1>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">
            <a href="/pertanyaan/create" class="btn btn-success">Pertanyaan Baru</a>
        </h3>

        <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                <div class="input-group-append">
                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover text-nowrap">
            <thead>
                <tr>
                    <th>Nomor</th>
                    <th>Judul</th>
                    <th>Isi Pertanyaan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach($questions as $key => $question)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td> {{ $question->judul }} </td>
                    <td>{{ $question->isi }} </td>
                    <td>
                        <a href="/pertanyaan/{{ $question->id }}" class="btn btn-info btn-sm">show</a>
                        <a href="/pertanyaan/{{ $question->id }}/edit" class="btn btn-dark btn-sm">edit</a>
                        <form action="/pertanyaan/{{ $question->id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->

</div>
@endsection