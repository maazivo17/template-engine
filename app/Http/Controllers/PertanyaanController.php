<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index()
    {
        $questions = DB::table('questions')->get();
        // dd($questions);
        return view('pertanyaan.index', compact('questions'));
    }


    public function create()
    {
        return view('pertanyaan.create');
    }


    public function store(Request $request)
    {
        $query = DB::table('questions')->insert([
            "judul" => $request["title"],
            "isi"   => $request["body"]
        ]);
        return redirect('/pertanyaan/create');
    }


    public function show($id)
    {
        $question = DB::table('questions')->where('id', $id)->first();
        // dd($question);
        return view('pertanyaan.show', compact('question'));
    }

    public function edit($id)
    {
        $question = DB::table('questions')->where('id', $id)->first();
        // dd($question);
        return view('pertanyaan.edit', compact('question'));
    }

    public function update($id, Request $request)
    {
        $query = DB::table('questions')->where('id', $id)->update([
            "judul" => $request["title"],
            "isi"   => $request["body"]
        ]);
        return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan!');
    }

    public function destroy($id)
    {
        $query = DB::table('questions')->where('id', $id)->delete();
        return redirect('/pertanyaan')->with('success', 'Berhasil hapus pertanyaan!');
    }
}
